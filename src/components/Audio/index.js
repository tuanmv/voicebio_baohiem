import React, { useEffect, useState } from "react";

import { Input, Row, Col, FormText, Button } from "reactstrap";
import {isNumber} from "chart.js/helpers";

function Audio({ file, setFile, isPlay, audioTime}) {
  const [urlAudio, setUrlAudio] = useState();
  const [fileNameAudio, setFileNameAudio] = useState("");

  const handleClickButton = () => {
    isPlay(false)
    document.getElementById("file_audio").click();
  };

  useEffect(() => {
    if (!file) {
      setUrlAudio();
    }
  }, [file]);

  const handleChangeFile = ({ target }) => {
    let _file = target?.files[0];
    if (_file?.name) {
      let url = URL.createObjectURL(_file);
      setUrlAudio(url);
      setFileNameAudio(_file?.name);
      setFile(_file);
    } else {
      setUrlAudio();
      setFileNameAudio("");
      setFile(null);
    }
  };

  const handlePlayAudio = () => {
    isPlay(!isPlay);
  }

  const timeUpdate = (event) => {
    var time = event.target.currentTime;
    audioTime(time);
  }

  return (
    <Row className="justify-content-center align-items-center mt-3">
      <Col xl="3">
        <Button
          color="primary"
          type="button"
          size="sm"
          outline
          onClick={handleClickButton}
        >
          Chọn tệp
          <Input
            hidden
            id="file_audio"
            name="file"
            type="file"
            accept="audio/,.mp3,.wav,.m4a"
            onChange={handleChangeFile}
          />
        </Button>
      </Col>
      <Col xl="6" className="d-flex align-items-center">
        {urlAudio ? (
          <div className="w-100">
            <audio onPlaying={handlePlayAudio} onPause={handlePlayAudio} onTimeUpdate={timeUpdate} controls className="w-100">
              <source  src={urlAudio} type="audio/ogg"></source>
            </audio>
            <FormText>Tên tệp: {fileNameAudio}</FormText>
          </div>
        ) : (
          <FormText>Hãy chọn tệp ghi âm</FormText>
        )}
      </Col>
    </Row>
  );
}

export default Audio;
